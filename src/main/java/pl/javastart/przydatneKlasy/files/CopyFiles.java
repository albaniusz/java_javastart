package pl.javastart.przydatneKlasy.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by filip on 5/13/18.
 */
public class CopyFiles {
    public static void main(String[] args) throws IOException {
        Files.copy(Paths.get("skąd"), Paths.get("dokąd"), StandardCopyOption.REPLACE_EXISTING);
    }
}
