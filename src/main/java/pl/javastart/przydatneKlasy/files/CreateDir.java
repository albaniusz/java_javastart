package pl.javastart.przydatneKlasy.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by filip on 5/13/18.
 */
public class CreateDir {
    public static void main(String[] args) throws IOException {
        Files.createDirectory(Paths.get("C:/nowyFolder"));
    }
}
