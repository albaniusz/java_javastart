package pl.javastart.przydatneKlasy.files;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by filip on 5/13/18.
 */
public class ReadAllLines {
    public static void main(String[] args) throws IOException {
        Files.readAllLines(Paths.get("C:/start/plik.txt"), Charset.forName("UTF-8"));
    }
}
