package pl.javastart.przydatneKlasy.klasyOslonowe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by filip on 5/13/18.
 */
public class Test {
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Podaj imie: ");
        String imie = br.readLine();

        System.out.println("Podaj wiek: ");
        int wiek = Integer.parseInt(br.readLine());
    }
}
