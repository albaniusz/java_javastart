package pl.javastart.przydatneKlasy.strings;

/**
 * Created by filip on 5/13/18.
 */
public class Strings2 {
    public static void main(String[] args) {
        String s = "a";
        long start = System.nanoTime();
        StringBuilder sB = new StringBuilder(s);

        for (int i = 0; i < 10000; i++) {
            sB.append("a");
        }

        s = sB.toString();
        System.out.println("Time2: " + (System.nanoTime() - start));
    }
}
