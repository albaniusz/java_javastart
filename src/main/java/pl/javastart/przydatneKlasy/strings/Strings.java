package pl.javastart.przydatneKlasy.strings;

/**
 * Created by filip on 5/13/18.
 */
public class Strings {
    public static void main(String[] args) {
        String s = "a";
        long start = System.nanoTime();
        StringBuffer strB = new StringBuffer(s);
        for (int i = 0; i < 10000; i++)
            strB.append("a");
        s = strB.toString();
        System.out.println("Time3: " + (System.nanoTime() - start));
    }
}
