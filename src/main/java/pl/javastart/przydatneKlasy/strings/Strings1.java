package pl.javastart.przydatneKlasy.strings;

/**
 * Created by filip on 5/13/18.
 */
public class Strings1 {
    public static void main(String[] args) {
        String s = "a";
        long start = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            s = s + "a";
//            s = new StringBuilder(s).append("a").toString();
        }
        System.out.println("Time1: " + (System.nanoTime() - start));
    }
}
