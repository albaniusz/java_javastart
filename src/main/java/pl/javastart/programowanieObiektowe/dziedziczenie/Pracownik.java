package pl.javastart.programowanieObiektowe.dziedziczenie;

/**
 * Created by filip on 5/12/18.
 */
class Pracownik {
    String imie;
    String nazwisko;
    int wyplata;

    public Pracownik() {
        imie = "";
        nazwisko = "";
        wyplata = 0;
    }

    public Pracownik(String i, String n, int w) {
        imie = i;
        nazwisko = n;
        wyplata = w;
    }
}
