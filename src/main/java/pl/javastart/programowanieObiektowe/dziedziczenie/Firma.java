package pl.javastart.programowanieObiektowe.dziedziczenie;

/**
 * Created by filip on 5/12/18.
 */
public class Firma {
    public static void main(String args[]) {
        Pracownik prac = new Pracownik("Wlodek", "Zięba", 3000);
        System.out.println("Imię: " + prac.imie);
        System.out.println("Nazwisko: " + prac.nazwisko);
        System.out.println("Wypłata: " + prac.wyplata + "\n");

        //najpierw stwórzmy domyślny obiekt klasy Szef korzystając z domyślnego konstruktora
        //odziedziconego z klasy Pracownik
        Szef szef = new Szef();

        //zobaczmy jak wyglądają odpowiednie pola
        System.out.println("Imię: " + szef.imie);
        System.out.println("Nazwisko: " + szef.nazwisko);
        System.out.println("Wypłata: " + szef.wyplata);
        System.out.println("Premia: " + szef.premia + "\n");

        //teraz ustawiamy dane szefa
        szef.imie = "Tadeusz";
        szef.nazwisko = "Kowalski";
        szef.wyplata = 10000;
        szef.premia = 2000;
        System.out.println("Imię: " + szef.imie);
        System.out.println("Nazwisko: " + szef.nazwisko);
        System.out.println("Wypłata: " + szef.wyplata);
        System.out.println("Premia: " + szef.premia);
    }
}
