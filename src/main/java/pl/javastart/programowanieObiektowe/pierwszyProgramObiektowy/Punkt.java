package pl.javastart.programowanieObiektowe.pierwszyProgramObiektowy;

/**
 * Created by filip on 5/11/18.
 */
public class Punkt {
    int wspX;
    int wspY;

    void ustawX(int x) {
        wspX = x;
    }

    void ustawY(int y) {
        wspY = y;
    }

    int dajX() {
        return wspX;
    }

    int dajY() {
        return wspY;
    }
}
