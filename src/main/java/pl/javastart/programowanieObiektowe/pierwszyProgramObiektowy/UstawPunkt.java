package pl.javastart.programowanieObiektowe.pierwszyProgramObiektowy;

/**
 * Created by filip on 5/11/18.
 */
public class UstawPunkt {
    public static void main(String args[]) {
        Punkt punkt = new Punkt();
        punkt.ustawX(10);
        punkt.ustawY(20);

        System.out.println("Współrzędne to: (" + punkt.dajX() + ", " + punkt.dajY() + ")");
    }
}
