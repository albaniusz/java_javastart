package pl.javastart.programowanieObiektowe.dziedziczeniecd;

/**
 * Created by filip on 5/12/18.
 */
public class Lekarz extends Pracownik {
    private double premia;

    public Lekarz(String imie, String nazwisko, double wyplata) {
        super(imie, nazwisko, wyplata);
        premia = 0;
    }

    public double getPremia() {
        return premia;
    }

    public void setPremia(double d) {
        premia = d;
    }
}
