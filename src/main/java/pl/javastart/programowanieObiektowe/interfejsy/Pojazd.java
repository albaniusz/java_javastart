package pl.javastart.programowanieObiektowe.interfejsy;

/**
 * Created by filip on 5/12/18.
 */
public interface Pojazd {
    public void jazda(int predkosc);

    public void stop();
}
