package pl.javastart.programowanieObiektowe.interfejsy;

/**
 * Created by filip on 5/12/18.
 */
public class Rower implements Pojazd {
    @Override
    public void jazda(int predkosc) {
    }

    @Override
    public void stop() {
    }

    public double skok() {
        return 0;
    }
}
