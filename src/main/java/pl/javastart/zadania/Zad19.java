package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.9 Pobierz w konsoli dwie liczby całkowite, następnie porównaj je i wyświetl stosowny komunikat z wynikiem.
 */
public class Zad19 {
    public static void main(String[] args) {
        int a;
        int b;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Liczba A: ");
        a = scanner.nextInt();

        System.out.println("Liczba B: ");
        b = scanner.nextInt();

        if (a > b) {
            System.out.println("Liczba A jest większa od B");
        } else if (a == b) {
            System.out.println("Liczba A jest rowna B");
        } else {
            System.out.println("Liczba A jest mniejsza od B");
        }
    }
}
