package pl.javastart.zadania;

import java.math.BigInteger;

import static java.lang.Math.pow;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.5 Napisz program analogiczny do 1.4 wykorzystując klasę BigInteger. Utwórz dwie liczby wykraczające poza zakres
 * long, wypróbuj funkcje dodawania, odejmowania i mnożenia, a także podniesienie do potęgi(zobacz w API jaki typ
 * parametru przyjmuje ta funkcja) i wartość bezwzględną. Wszystkie wyniki zaprezentuj w konsoli.
 */
public class Zad15 {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("3483849384983");
        BigInteger b = new BigInteger("2083984829");

        System.out.println(a.add(b));
        System.out.println(a.mod(b));
        System.out.println(a.multiply(b));
        System.out.println(a.divide(b));
        System.out.println(a.abs());
    }
}
