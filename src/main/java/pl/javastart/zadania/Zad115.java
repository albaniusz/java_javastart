package pl.javastart.zadania;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.15 Do pliku daneBinarne.txt zapisz swoje imię, aktualny rok i wynik dzielenia 50/4. Następnie z tego samego pliku
 * odczytaj ile znajduje się w nim bajtów.
 */
public class Zad115 {
    public static void main(String[] args) {
        String fileName = "resources/daneBinarne.txt";
        String access = "rw";

        try {
            RandomAccessFile stream = new RandomAccessFile(fileName, access);

            stream.writeUTF("Hans Solo");
            stream.writeInt(2018);
            stream.writeDouble(50 / 4);
            stream.close();

            stream = new RandomAccessFile(fileName, access);

            int bytes = 0;
            while (stream.read() != -1)
                bytes++;

            stream.close();

            System.out.println("Ilość bajtów: " + bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
