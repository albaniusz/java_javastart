package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.11 Napisz program, który pobierze od użytkownika całkowitą liczbę dodatnią. Następnie przy użyciu wyświetl
 * na ekranie Odliczanie z tekstem "Bomba wybuchnie za ... " gdzie w miejsce dwukropka mają się pojawić liczby
 * od podanej przez użytkownika do 0. Napisz program w 3 wersjach przy użyciu różnych pętli.
 */
public class Zad111 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę całkowitą: ");
        int number = scanner.nextInt();

        int counter;
        String message = "Bomba wybuchnie za ... ";

        counter = number;
        while (counter >= 0) {
            System.out.println(message + counter);
            counter--;
        }

        counter = number;
        do {
            System.out.println(message + counter);
            counter--;
        } while (counter >= 0);

        for (int x = number; x >= 0; x--) {
            System.out.println("Bomba wybuchnie za ... " + x);
        }
    }
}
