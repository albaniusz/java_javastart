package pl.javastart.zadania.zad21;

/**
 * Created by filip on 5/11/18.
 * <p/>
 * Napisz klasę Pracownik, która przechowuje trzy pola:
 * -Imię
 * -Nazwisko
 * -Wiek
 * <p/>
 * Następnie utwórz klasę Firma, w której wykorzystasz klasę pracownik do utworzenia dwóch obiektów przechowujących
 * dane pracowników (wymyśl sobie jakieś) i później wyświetlasz je na ekran.
 */
public class Firma {
    private static void printData(Pracownik emp) {
        System.out.println("Imię: " + emp.getName() + "; Nazwisko: " + emp.getSurname() + "; Wiek: " + emp.getAge());
    }

    public static void main(String[] args) {
        Pracownik emp1 = new Pracownik();
        emp1.setName("Jan");
        emp1.setSurname("Kowalski");
        emp1.setAge(43);

        Pracownik emp2 = new Pracownik();
        emp2.setName("Piotr");
        emp2.setSurname("Nowak");
        emp2.setAge(36);

        printData(emp1);
        printData(emp2);
    }
}
