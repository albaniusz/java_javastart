package pl.javastart.zadania.zad26;

/**
 * Created by filip on 5/12/18.
 */
public class Punkt2D {
    protected int x;
    protected int y;

    public Punkt2D() {
        setX(0);
        setY(0);
    }

    public Punkt2D(int x, int y) {
        setX(x);
        setY(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return "X: " + getX() + ", Y: " + getY();
    }
}
