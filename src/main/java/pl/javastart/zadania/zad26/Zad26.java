package pl.javastart.zadania.zad26;

/**
 * Created by filip on 5/12/18.
 * <p/>
 * 2.6 Stwórz klasę Punkt2D, która przechowuje informacje na temat punktu na przestrzeni dwuwymiarowej (współrzędne
 * x oraz y). Zawierająca dwa konstruktory: bezparametrowy ustawiający pola na wartość 0, oraz przyjmujący dwa argumenty
 * i ustawiający pola obiektu zgodnie z podanymi parametrami.
 * <p/>
 * Napisz klasę Punkt3D dziedziczącą po Punkt2D, reprezentującą punkt w trójwymiarze (dodatkowe pole z).
 * <p/>
 * W klasie testowej utwórz obiekty obu klas i przetestuj działanie.
 */
public class Zad26 {
    public static void main(String[] args) {
        Punkt2D punkt2D = new Punkt2D();
        Punkt3D punkt3D = new Punkt3D(1, 2, 3);

        System.out.println(punkt2D);
        System.out.println(punkt3D);
    }
}
