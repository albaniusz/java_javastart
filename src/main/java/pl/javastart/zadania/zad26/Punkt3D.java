package pl.javastart.zadania.zad26;

/**
 * Created by filip on 5/12/18.
 */
public class Punkt3D extends Punkt2D {
    protected int z;

    public Punkt3D() {
        super();
        setZ(0);
    }

    public Punkt3D(int x, int y) {
        super(x, y);
        setZ(0);
    }

    public Punkt3D(int x, int y, int z) {
        super(x, y);
        setZ(z);
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public String toString() {
        return "X: " + getX() + ", Y: " + getY() + ", Z: " + getZ();
    }
}
