package pl.javastart.zadania;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.2. Napisz program, w którym zadeklarujesz kilka zmiennych finalnych, lub zmiennych różnych typów o dowolnych
 * nazwach, a następnie wyświetlisz je w kolejnych liniach tekstu. Skompiluj, lub spróbuj skompilować przykłady podane
 * w tej lekcji i zobacz co się stanie przy próbie nadania po raz drugi wartości jakiejś zmiennej finalnej.
 * <p/>
 * W tym samym programie zadeklaruj cztery zmienne typu String. Trzy z nich zainicjuj jakimiś wyrazami a czwartemu
 * przypisz ciąg znaków utworzony z trzech wcześniejszych zmiennych. Następnie wyświetl czwartą zmienną na ekranie.
 * <p/>
 * Przy użyciu metody substring wyświetl na ekranie dwa pierwsze wyrazy wykorzystując wyłącznie czwartą zmienną typu
 * String.
 */
public class Zad12 {
    public static void main(String[] args) {
        final int ALFA = 12;
        final char BETA = 'a';
        final boolean GAMA = true;

        String string1 = "Lorem";
        String string2 = "Ipsum";
        String string3 = "Dolor";
        String string4 = string1 + string2 + string3;

        System.out.println(string4);
        System.out.println(string4.substring(0, (string1.length() + string2.length())));
    }
}
