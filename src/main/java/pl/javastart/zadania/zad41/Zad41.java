package pl.javastart.zadania.zad41;

import java.awt.*;

/**
 * Created by filip on 5/12/18.
 */
public class Zad41 {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PictureFrame();
            }
        });
    }
}
