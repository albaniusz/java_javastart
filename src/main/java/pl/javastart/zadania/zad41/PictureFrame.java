package pl.javastart.zadania.zad41;

import javax.swing.*;

/**
 * Created by filip on 5/12/18.
 */
public class PictureFrame extends JFrame {
    public PictureFrame() {
        super("Zadanie 4.1");

        JPanel panel = new PicturePanel();
        add(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
