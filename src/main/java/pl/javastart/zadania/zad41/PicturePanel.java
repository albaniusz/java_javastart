package pl.javastart.zadania.zad41;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by filip on 5/12/18.
 */
public class PicturePanel extends JPanel {
    private BufferedImage image1;
    private BufferedImage image2;

    public PicturePanel() {
        super();

        try {
            image1 = ImageIO.read(new File("resources/java.png"));
            image2 = ImageIO.read(new File("resources/idea.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Dimension dimension = new Dimension(image1.getWidth() * 2, image1.getHeight());
        setPreferredSize(dimension);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(image1, 0, 0, this);
        g2d.drawImage(image2, image1.getWidth(), 0, this);
    }
}
