package pl.javastart.zadania;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.6 Wykorzystaj konwersję i rzutowanie wypróbuj zamiany różnych typów prostych między sobą. Szczególną uwagę zwróć
 * na rzutowanie char na int. Jak myślisz, co w ten sposób otrzymujesz?
 */
public class Zad16 {
    public static void main(String[] args) {
        int a = 5;
        double b = 13.5;
        double c = b / (double) a;
    }
}
