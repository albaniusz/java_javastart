package pl.javastart.zadania.zad24;

/**
 * Created by filip on 5/11/18.
 */
public class Toolkit {
    public void dodaj(int x, double y) {
        System.out.println("Dodaj int double: " + (x + y));
    }

    public void dodaj(int x, int y, int z) {
        System.out.println("Dodaj int int int: " + (x + y + z));
    }

    public void dodaj(long x, double y, int z) {
        System.out.println("Dodaj long double int: " + (x + y + z));
    }

    public void dodaj(String label, int x, int y, int z) {
        System.out.println("Dodaj String int int int: (" + label + ") " + (x + y + z));
    }

    public void dodaj(double x, double y) {
        System.out.println("Dodaj double double: " + (x + y));
    }
}
