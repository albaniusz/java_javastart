package pl.javastart.zadania.zad24;

/**
 * Created by filip on 5/11/18.
 * <p/>
 * 2.4 Napisz program składający się z dwóch klas. Pierwsza niech zawiera kilka metod o nazwie dodaj(), ale zwracających
 * różne typy wyników i przyjmujących po minimum dwa parametry typów liczbowych wybranych przez Ciebie. Ich zadaniem
 * jest zwrócenie, lub wyświetlanie sumy podanych argumentów. W drugiej klasie Testowej utwórz obiekt tej klasy
 * i sprawdź działanie swoich metod, wyświetlając wyniki działań na ekranie. Dodatkowo każda z metod niech wyświetla
 * swój typ zwracany i rodzaj argumentów, abyś wiedział, która z nich zadziałała.
 */
public class Zad24 {
    public static void main(String[] args) {
        Toolkit toolkit = new Toolkit();

        toolkit.dodaj(1, 1.0);
        toolkit.dodaj(1, 2, 3);
        toolkit.dodaj(1213313, 12.0, 1);
        toolkit.dodaj("LABEL", 1, 2, 3);
        toolkit.dodaj(1212.0, 12);
    }
}
