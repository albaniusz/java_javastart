package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.7 Utwórz dwie zmienne typu double. Następnie przy użyciu klasy Scanner pobierz od użytkownika dwie liczby
 * i wykonaj na nich dodawanie, odejmowanie, mnożenie i dzielenie, wyświetlając wyniki w kolejnych liniach na konsoli.
 */
public class Zad17 {
    public static void main(String[] args) {
        double a;
        double b;

        Scanner scaner = new Scanner(System.in);

        System.out.println("Podaj liczbe A:");
        a = scaner.nextDouble();

        System.out.println("Podaj liczbe B:");
        b = scaner.nextDouble();

        System.out.println("Dodawanie: " + (a + b));
        System.out.println("Odejmowanie: " + (a - b));
        System.out.println("Mnożenie: " + (a * b));
        System.out.println("Dzielenie: " + (a / b));
    }
}
