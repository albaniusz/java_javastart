package pl.javastart.zadania;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.8 Napisz program, w którym wprowadzisz w konsoli swoje imię, następnie zapiszesz je do pliku. Odczytaj je
 * z powrotem z pliku i bez użycia dodatkowej zmiennej wyświetl na ekranie.
 */
public class Zad18 {
    public static void main(String[] args) throws FileNotFoundException {
        String fileName = "resources/zad18.txt";

        System.out.println("Podaj swoje imie: ");

        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        PrintWriter writer = new PrintWriter(fileName);
        writer.println(name);
        writer.close();

        Scanner reader = new Scanner(new File(fileName));
        System.out.println(reader.nextLine());
    }
}
