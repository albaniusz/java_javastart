package pl.javastart.zadania;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.3 Napisz prosty kalkulator, w którym będziesz przechowywał 3 zmienne typu double o nazwach a,b,c. Wypróbuj
 * wszystkie operatory matematyczne:
 * (a+b)*c
 * a-b/c
 * użyj operatorów inkrementacji i zwiększ wszystkie zmienne o 1.
 * <p/>
 * Teraz porównaj ze sobą, czy:
 * (a+b)>c
 * czy a=b?
 * <p/>
 * Przedstaw wyniki w konsoli.
 */
public class Zad13 {
    public static void main(String[] args) {
        double a = 1;
        double b = 2;
        double c = 3;

        System.out.println((a + b) * c);
        System.out.println(a - b / c);

        a++;
        b++;
        c++;

        System.out.println((a + b) > c ? true : false);
        System.out.println(a == b ? true : false);

        System.out.println("a: " + a);
        System.out.println("b: " + b);
        System.out.println("c: " + c);
    }
}
