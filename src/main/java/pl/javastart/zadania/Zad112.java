package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.12 Napisz program, w którym zadeklarujesz i utworzysz pięcioelementową tablicę odpowiedniego typu. W pętli
 * pobierzesz od użytkownika 5 imion i je w niej zapiszesz. Następnie wyświetl na ekranie powiadomienia "Witaj imie_
 * z_tablicy" dla każdego z podanych parametrów.
 */
public class Zad112 {
    public static void main(String[] args) {
        String names[] = new String[5];

        Scanner scanner = new Scanner(System.in);

        for (int x = 0; x < 5; x++) {
            System.out.println("Podaj imię nr. " + (x + 1));
            names[x] = scanner.nextLine();
        }

        for (int x = 0; x < 5; x++) {
            System.out.println("Witaj " + names[x]);
        }
    }
}
