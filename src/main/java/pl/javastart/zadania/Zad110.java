package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.10 Napisz program, który pobierze od użytkownika imię i przechowa je w zmiennej. Następnie stwórz kilka warunków
 * z różnymi imionami. Jeśli któreś z imion będzie pasowało wyświetl "Cześć jakieś_imię", gdy program nie znajdzie
 * imienia wyświetl "Przykro mi, ale Cię nie znam".
 */
public class Zad110 {
    public static void main(String[] args) {
        String name;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie: ");
        name = scanner.nextLine();

        String result = "Cześć ";

        if (name.equals("Marek")
                || name.equals("Darek")
                || name.equals("Franek")
                || name.equals("Krzyś")
                || name.equals("Witek")) {
            result += name;
        } else {
            result = "Przykro mi, ale Cię nie znam";
        }

        System.out.println(result);
    }
}
