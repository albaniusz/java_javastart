package pl.javastart.zadania;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.1. Napisz program, który wyświetli w 3 kolejnych liniach trzy imiona: Ania, Bartek, Kasia.
 */
public class Zad11 {

    public static void main(String[] args) {
        System.out.println("Ania");
        System.out.println("Bartek");
        System.out.println("Kasia");
    }
}
