package pl.javastart.zadania.zad25;

/**
 * Created by filip on 5/12/18.
 */
public class Pracownik {
    /**
     * imię
     */
    private String name;
    /**
     * nazwisko
     */
    private String surname;
    /**
     * wiek
     */
    private int age;

    public Pracownik() {
        setName("Joe");
        setSurname("Doe");
        setAge(0);
    }

    public Pracownik(String name, String surname) {
        setName(name);
        setSurname(surname);
        setAge(0);
    }

    public Pracownik(String name, String surname, int age) {
        setName(name);
        setSurname(surname);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return getName() + " " + getSurname() + ", " + getAge();
    }
}
