package pl.javastart.zadania.zad25;

/**
 * Created by filip on 5/12/18.
 * <p/>
 * 2.5 Napisz program, który będzie się składał z dwóch klas:
 * <p/>
 * -Pracownik - przechowująca takie dane jak imię, nazwisko i wiek pracownika, oraz co najmniej trzy konstruktory,
 * które posłużą do inicjowania obiektów z różnymi parametrami - w przypadku gdy przykładowo konstruktor przyjmuje
 * tylko 1 parametr, zainicjuj pozostałe pola jakimiś domyślnymi wartościami.
 * -Firma - klasa testowa, w której utworzysz kilka obiektów klasy Pracownik i wyświetlisz dane na ekran
 * <p/>
 * Mile widziane wykorzystanie tablic oraz pętli.
 */
public class Firma {
    public static void main(String[] args) {
        Pracownik emp1 = new Pracownik();
        System.out.println(emp1);

        Pracownik emp2 = new Pracownik("Jan", "Kowalski");
        System.out.println(emp2);

        Pracownik emp3 = new Pracownik("Ernest", "Buhenwaltz", 34);
        System.out.println(emp3);
    }
}
