package pl.javastart.zadania;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.14 Przy użyciu pętli i tablic przechowujących liczby całkowite zaprezentuj poniższą treść:
 * tab[0,0] = 0;
 * tab[0,1] = 1;
 * tab[0,2] = 2;
 * tab[1,0] = 3;
 * tab[1,1] = 4;
 * tab[1,2] = 5;
 * Wykorzystuj przy tym własność length.
 */
public class Zad114 {
    public static void main(String[] args) {
        int tab[][] = new int[2][3];

        tab[0][0] = 0;
        tab[0][1] = 1;
        tab[0][2] = 2;
        tab[1][0] = 3;
        tab[1][1] = 4;
        tab[1][2] = 5;

        for (int x = 0; x < tab.length; x++) {
            int subTab[] = tab[x];
            for (int y = 0; y < subTab.length; y++) {
                System.out.println("tab[" + x + "." + y + "] = " + tab[x][y]);
            }
        }
    }
}
