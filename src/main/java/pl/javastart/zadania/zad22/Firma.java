package pl.javastart.zadania.zad22;

/**
 * Created by filip on 5/11/18.
 * <p/>
 * 2.2 Zmodyfikuj powyższy program tak, aby utworzyć trzech pracowników, a odpowiednie pola zainicjuj wartościami
 * z wcześniej utworzonych tablic (dowolne dane) przy użyciu pętli.
 */
public class Firma {
    public static void main(String[] args) {
        Pracownik[] empl = new Pracownik[3];

        String[] names = {"Adam", "Borys", "Cyprian"};
        String[] surnames = {"Adler", "Białczak", "Cętka"};
        int[] age = {34, 52, 31};

        for (int x = 0; x < empl.length; x++) {
            empl[x] = new Pracownik();
            empl[x].setName(names[x]);
            empl[x].setSurname(surnames[x]);
            empl[x].setAge(age[x]);
        }

        for (int i = 0; i < empl.length; i++) {
            System.out.println(empl[i].getName() + " " + empl[i].getSurname() + ", " + empl[i].getAge() + " lat");
        }
    }
}
