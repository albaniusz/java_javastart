package pl.javastart.zadania;

import java.io.*;

/**
 * Created by filip on 5/11/18.
 * <p/>
 * 1.16 Korzystając z własnego buforowania przekopiuj dane znajdujące się w pliku binarnie.txt do przekopiowane.txt.
 * Dodatkowo zlicz ilość skopiowanych bajtów.
 */
public class Zad116 {
    public static void main(String[] args) {
        DataInputStream streamIn;
        DataOutputStream streamOut;

        try {
            streamIn = new DataInputStream(new FileInputStream("resources/binarnie.txt"));
            streamOut = new DataOutputStream(new FileOutputStream("resources/przekopiowane.txt"));

            int bytesCount = 0;
            int bytesTotal = 0;
            byte[] buffer = new byte[1024];

            while ((bytesCount = streamIn.read(buffer)) != -1) {
                streamOut.write(buffer, 0, bytesCount);
                bytesTotal += bytesCount;
            }

            if (streamIn != null)
                streamIn.close();
            if (streamOut != null)
                streamOut.close();

            System.out.println(bytesTotal);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
