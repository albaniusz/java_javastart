package pl.javastart.zadania;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 * <p/>
 * 1.13 Utwórz tablicę typu int przechowującą n elementów - gdzie n jest parametrem pobieranym od użytkownika.
 * Następnie wypełnij ją liczbami od 1 do n i wyświetl zawartość na ekranie przy pomocy pętli while.
 */
public class Zad113 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę: ");
        int number = scanner.nextInt();

        int array[] = new int[number];

        for (int x = 0; x < number; x++) {
            array[x] = x + 1;
        }

        int x = 0;
        while (x < number) {
            System.out.println(array[x]);
            x++;
        }
    }
}
