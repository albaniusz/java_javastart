package pl.javastart.zadania.zad23;

/**
 * Created by filip on 5/11/18.
 * <p/>
 * 2.3 Utwórz klasę Punkt, która przechowuje dwie wartości typu int - współrzędne punktu na powierzchni. Napisz w niej
 * także metody które:
 * zwiększają wybraną współrzędną o 1
 * zmieniają wybraną zmienną o dowolną wartość
 * zwracają wartość współrzędnych (oddzielne metody)
 * wyświetla wartość współrzędnych
 * <p/>
 * Napisz także klasę, w której przetestujesz działanie metod wyświetlając działanie metod na ekranie,
 */
public class Zad23 {
    public static void main(String[] args) {
        Punkt point = new Punkt();

        point.setX(1);
        point.setY(2);
        point.showValues();

        point.increaseX();
        point.increaseY();
        point.showValues();

        point.changeXbyValue(4);
        point.changeYbyValue(2);
        point.showValues();

        System.out.println("Wartość X: " + point.getX());
        System.out.println("Wartość Y: " + point.getY());
    }
}
