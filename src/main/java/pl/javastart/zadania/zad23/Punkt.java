package pl.javastart.zadania.zad23;

/**
 * Created by filip on 5/11/18.
 */
public class Punkt {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void increaseX() {
        x++;
    }

    public void increaseY() {
        y++;
    }

    public void changeXbyValue(int value) {
        x += value;
    }

    public void changeYbyValue(int value) {
        y += value;
    }

    public void showValues() {
        System.out.println("X: " + x + "; Y: " + y);
    }
}
