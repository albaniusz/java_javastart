package pl.javastart.algorytmyStruktury;

/**
 * Created by filip on 5/14/18.
 */
public class Fib2 {
    public static void main(String[] args) {

        long time1 = System.nanoTime();

        for (int i = 2; i <= 154; i++) {
            System.out.println(i + " wyraz: " + fibo2(i));
        }

        long time2 = System.nanoTime();
        System.out.println("Całkowity czas obliczeń: " + (time2 - time1) / 1000000 + " milisekund");

    }

    public static long fibo2(int arg) {
        if ((arg == 1) || (arg == 2))
            return 1;

        long[] F = new long[155];

        F[0] = 0;
        F[1] = 1;

        int i = 0;

        for (i = 2; i <= arg; i++) {
            F[i] = F[i - 1] + F[i - 2];
        }

        return F[i - 1];
    }
}
