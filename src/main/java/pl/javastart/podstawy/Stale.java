package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class Stale {
    public static void main(String[] args) {
        final double liczbaPi = 3.14;
        final int finalna2;
        //liczbaPi = 8;     //błąd, zmienna była zainicjowana
        finalna2 = 3;    // tak można zrobić - pierwsze przypisanie
    }
}
