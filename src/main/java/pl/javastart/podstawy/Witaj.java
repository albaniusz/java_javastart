package pl.javastart.podstawy;

import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 */
public class Witaj {
    public static void main(String[] args){
        String imie; //w nim zapiszemy swoje imie
        Scanner odczyt = new Scanner(System.in); //obiekt do odebrania danych od użytkownika

        imie = odczyt.nextLine();

        System.out.println("Witaj "+imie); //wyświetlamy powitanie
    }
}
