package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class TestSwitch {
    public static void main(String[] args) {
        int zmienna = 3;

        switch (zmienna) {
            case 1:
                System.out.println("Liczba to 1");
                break;
            case 2:
                System.out.println("Liczba to 2");
                break;
            default:
                System.out.println("Liczba to ani 1, ani 2");
        }
    }
}
