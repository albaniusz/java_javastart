package pl.javastart.podstawy;

import java.math.BigInteger;

/**
 * Created by filip on 5/10/18.
 */
public class WielkaLiczba {
    public static void main(String[] args) {
        BigInteger wielkaLiczba = new BigInteger("12312312312312312312");
        System.out.println(wielkaLiczba.toString());
    }
}
