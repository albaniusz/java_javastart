package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class Operatory {
    public static void main(String[] args) {
        int a = 17;
        int b = 4;
        int c = a + b; //=21
        c = a - b;     //=13
        c = a * b;     //=68
        c = a / b;     //=4 ponieważ 4*4=16 i zostaje reszty 1
        c = a % b;     //=1 reszta z dzielenia

        boolean prawda = a > b; //prawda=true
        boolean falsz = a < b;  //falsz=false
        boolean porownanie = a == b;  //porownanie=false
        boolean koniunkcja = (a > b) && (a != b); //true prawda i prawda = prawda

        String ja = "Slawek";
        String ty = "Slawek";

        boolean porownanie1 = ja == ty;
        boolean porownanie2 = ja.equals(ty);
        System.out.println(porownanie1);
        System.out.println(porownanie2);
    }
}
