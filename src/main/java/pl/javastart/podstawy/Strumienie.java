package pl.javastart.podstawy;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by filip on 5/10/18.
 */
public class Strumienie {
    public static void main(String[] args) {
        try {
            DataOutputStream strumien = new DataOutputStream(new FileOutputStream("resources/strumienie.txt"));
            /*
			 * Dowolne
			 * metody
			 *
			 */
            strumien.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        } catch (IOException e) {
            System.out.println("Błąd wejścia-wyjścia");
        }
    }
}
