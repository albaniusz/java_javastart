package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class Substring {
    public static void main(String[] args) {
        String hello = "Witaj ";
        String world = "Świecie!";
        String powitanie = hello + world; //łączenie Stringów
        System.out.println(powitanie);

        String czesc = powitanie.substring(0, 6) + "uczniu";
        System.out.println(czesc);
    }
}
