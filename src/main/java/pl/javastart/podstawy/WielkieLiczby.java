package pl.javastart.podstawy;

import java.math.BigInteger;

/**
 * Created by filip on 5/10/18.
 */
public class WielkieLiczby {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("123123123123123123123123123123");
        BigInteger b = new BigInteger("987654321987654321987654321987");
        BigInteger suma = a.add(b);
        System.out.println("Suma " + suma.toString());
    }
}
