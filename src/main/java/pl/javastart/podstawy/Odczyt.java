package pl.javastart.podstawy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by filip on 5/10/18.
 */
public class Odczyt {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("resources/odczyt.txt");
        Scanner in = new Scanner(file);

        String zdanie = in.nextLine();
        System.out.println(zdanie);
    }
}
