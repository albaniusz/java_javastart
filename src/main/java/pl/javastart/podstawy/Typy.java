package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class Typy {
    public static void main(String[] args) {
        System.out.println('a' + 'A');
        System.out.println(1 + 2);
        System.out.println(1.0 + 2.0);
        System.out.println("cudzysłów \"");
        System.out.println(true);
    }
}
