package pl.javastart.podstawy;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by filip on 5/10/18.
 */
public class Zapis {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter zapis = new PrintWriter("resources/zapis.txt");
        zapis.println("Ala ma kota, a kot ma Alę");
        zapis.close();
    }
}
