package pl.javastart.podstawy;

/**
 * Created by filip on 5/10/18.
 */
public class Tablice {
    public static void main(String[] args) {
        int[] tablica = new int[10];

        for (int i = 0; i < 10; i++)
            tablica[i] = i + 1;

        int zmienna = tablica[3];

        for (int i = 0; i < 10; i++)
            System.out.println("Kolejna komórka to: " + tablica[i]);
    }
}
