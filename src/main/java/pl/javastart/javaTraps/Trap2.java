package pl.javastart.javaTraps;

import java.util.Random;

/**
 * Created by filip on 5/13/18.
 */
public class Trap2 {
    public static void main(String args[]) {
        Random rand = new Random();
        boolean check = rand.nextBoolean();

        Number number = (check || !check) ? new Integer(1) : new Double(2.0);

        System.out.println("Wynik: " + number);
    }
}
