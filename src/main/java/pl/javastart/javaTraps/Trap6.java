package pl.javastart.javaTraps;

/**
 * Created by filip on 5/13/18.
 */
public class Trap6 {
    private int x = 0;

    public Trap6(int x) {
        this.x = x;
    }

    public boolean equal(Trap6 p) {
        return this.x == p.x;
    }

    public static void main(String[] args) {
        Trap6 a = new Trap6(1);
        Trap6 b = new Trap6(1);

        System.out.print(a.equal(b) + " ");
        System.out.print(a.x == b.x);
    }
}
