package pl.javastart.javaTraps;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by filip on 5/13/18.
 */
public class Trap3 {
    public static void main(String[] args) throws MalformedURLException {
        Set set = new HashSet();
        set.add(new URL("http://wolnifarmerzy.com.pl"));
        set.add(new URL("http://zagubionawyspa.com.pl"));
        set.add(new URL("http://editor.javastart.pl"));
        set.add(new URL("http://google.pl"));

        System.out.println("Size: " + set.size());
    }
}
