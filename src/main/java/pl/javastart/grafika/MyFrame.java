package pl.javastart.grafika;

import javax.swing.*;
import java.awt.*;

/**
 * Created by filip on 5/12/18.
 */
public class MyFrame extends JFrame {

    public MyFrame() {
        super("Hello World");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setSize(300, 100);
        setLocation(50, 50);

        setLayout(new GridLayout(2, 6));

        for (int i = 0; i < 10; i++)
            add(new JButton("" + (i + 1)));

        setVisible(true);
    }
}
