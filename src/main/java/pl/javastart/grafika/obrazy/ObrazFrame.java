package pl.javastart.grafika.obrazy;

import javax.swing.*;

/**
 * Created by filip on 5/12/18.
 */
public class ObrazFrame extends JFrame {

    public ObrazFrame() {
        super("Program obrazkowy");

        JPanel obrazPanel = new ObrazPanel();
        add(obrazPanel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
