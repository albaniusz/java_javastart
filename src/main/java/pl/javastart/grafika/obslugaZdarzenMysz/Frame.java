package pl.javastart.grafika.obslugaZdarzenMysz;

import javax.swing.*;
import java.awt.*;

/**
 * Created by filip on 5/12/18.
 */
public class Frame extends JFrame {
    public Frame() {
        super("MouseTest");

        add(new MouseTestPanel());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Frame();
            }
        });
    }
}
