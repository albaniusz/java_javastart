package pl.javastart.grafika.obslugaZdarzenPrzyciski;

import javax.swing.*;

/**
 * Created by filip on 5/12/18.
 */
public class ActionFrame extends JFrame {
    public ActionFrame() {
        super("Test akcji");

        JPanel buttonPanel = new ButtonPanel();
        add(buttonPanel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
