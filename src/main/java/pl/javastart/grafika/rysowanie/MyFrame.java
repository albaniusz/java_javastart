package pl.javastart.grafika.rysowanie;

import javax.swing.*;

/**
 * Created by filip on 5/12/18.
 */
public class MyFrame extends JFrame {
    public MyFrame() {
        super("Rysowanie");
        JPanel panel = new MyPanel();

        add(panel);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
