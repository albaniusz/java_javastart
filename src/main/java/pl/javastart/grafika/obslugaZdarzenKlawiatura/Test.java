package pl.javastart.grafika.obslugaZdarzenKlawiatura;

import java.awt.*;

/**
 * Created by filip on 5/12/18.
 */
public class Test {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new KeyTest();
            }
        });
    }
}
