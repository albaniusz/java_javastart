package pl.javastart.zaawansowaneProgramowanie.calculator;

import java.awt.*;

/**
 * Created by filip on 5/15/18.
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CalcFrame();
            }
        });
    }
}
