package pl.javastart.zaawansowaneProgramowanie.bazyDanych;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 * Created by filip on 5/15/18.
 */
public class PerformanceTest {
    public static void main(String[] args) throws Exception {
        Class.forName("org.sqlite.JDBC");

        Connection conn = DriverManager.getConnection("jdbc:sqlite:test1.db");
        Statement stat = conn.createStatement();
        stat.execute("CREATE TABLE users (id INTEGER, name varchar(255))");

        long start = System.currentTimeMillis();
        PreparedStatement prepStmt = conn
                .prepareStatement("INSERT INTO users values (?, ?);");
        for (int i = 1; i <= 100; i++) {
            prepStmt.setInt(1, i);
            prepStmt.setString(2, "someName" + i);
            prepStmt.executeUpdate();
        }
        System.out.println("Time1: " + (System.currentTimeMillis() - start));
        conn.close();

        Connection conn2 = DriverManager.getConnection("jdbc:sqlite:test2.db");
        conn2.setAutoCommit(false);
        Statement stat2 = conn2.createStatement();
        stat2.execute("CREATE TABLE users (id INTEGER, name varchar(255))");

        start = System.currentTimeMillis();
        PreparedStatement prepStmt2 = conn2
                .prepareStatement("INSERT INTO users values (?, ?);");
        for (int i = 1; i <= 100; i++) {
            prepStmt2.setInt(1, i);
            prepStmt2.setString(2, "someName" + i);
            prepStmt2.addBatch();
        }
        prepStmt2.executeBatch();
        conn2.commit();
        System.out.println("Time2: " + (System.currentTimeMillis() - start));
        conn2.close();
    }
}
